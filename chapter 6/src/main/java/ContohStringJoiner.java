import java.util.StringJoiner;

public class ContohStringJoiner {
    public static void main(String[] args) {
        contohJoiner();
    }

    public static void contohJoiner(){
        String[] rgbList = new String[3];
        rgbList[0] = "Red";
        rgbList[1] = "Green";
        rgbList[2] = "Blue";

        StringJoiner rgbJoiner = new StringJoiner(",");
        for (int index = 0; index < rgbList.length; index++) {
            rgbJoiner.add(rgbList[index]);
        }
        System.out.println(rgbJoiner.toString());
    }
}
