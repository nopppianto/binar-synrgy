import java.time.format.DateTimeFormatter;

public class LocalDateTimeFormater {
    public static void main(String[] args) {
        LocalDateTimeFormater now = LocalDateTimeFormater.now();

        System.out.println("Before : " + now);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        String formatDateTime = now.format(formatter);

        System.out.println("After : " + formatDateTime);
    }
}
