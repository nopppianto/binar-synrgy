import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Kalkulator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char operator;
        double hasil = 0.0;
        List<String> riwayat = new ArrayList<>();

        try (BufferedWriter penulis = new BufferedWriter(new FileWriter("riwayat.txt", true))) {
            riwayat = bacaRiwayatDariFile("riwayat.txt");

            while (true) {
                tampilkanMenuOperasi();
                int pilihan = input.nextInt();

                if (pilihan == 8) {
                    break;
                }

                if (pilihan < 1 || pilihan > 8) {
                    System.out.println("Operasi tidak valid. Silakan coba lagi.");
                    continue;
                }

                if (pilihan == 5) {
                    cetakRiwayatKeFile(riwayat, penulis);
                    continue;
                } else if (pilihan == 6) {
                    tampilkanRiwayat(riwayat);
                    continue;
                } else if (pilihan == 7) {
                    double total = hitungTotal(riwayat);
                    System.out.println("Total hasil semua operasi: " + total);
                    continue;
                }

                System.out.print("Masukkan angka pertama: ");
                double angka1 = input.nextDouble();
                System.out.print("Masukkan angka kedua: ");
                double angka2 = input.nextDouble();

                operator = getSimbolOperator(pilihan);
                hasil = hitungOperasi(operator, angka1, angka2);
                String operasi = angka1 + " " + operator + " " + angka2 + " = " + hasil;
                riwayat.add(operasi);

                System.out.println("Hasil: " + hasil);
            }
        } catch (IOException e) {
            System.out.println("Gagal menulis ke file riwayat.txt.");
        }
    }

    public static void tampilkanMenuOperasi() {
        System.out.println("Pilih operasi:");
        System.out.println("1. Penambahan (+)");
        System.out.println("2. Pengurangan (-)");
        System.out.println("3. Perkalian (*)");
        System.out.println("4. Pembagian (/)");
        System.out.println("5. Cetak Riwayat ke File");
        System.out.println("6. Tampilkan Riwayat di Terminal");
        System.out.println("7. Lihat Total Hasil Operasi");
        System.out.println("8. Keluar");
        System.out.print("Masukkan nomor operasi (1/2/3/4/5/6/7/8): ");
    }

    public static char getSimbolOperator(int pilihan) {
        switch (pilihan) {
            case 1:
                return '+';
            case 2:
                return '-';
            case 3:
                return '*';
            case 4:
                return '/';
            default:
                return ' ';
        }
    }

    public static double hitungOperasi(char operator, double angka1, double angka2) {
        switch (operator) {
            case '+':
                return angka1 + angka2;
            case '-':
                return angka1 - angka2;
            case '*':
                return angka1 * angka2;
            case '/':
                if (angka2 != 0) {
                    return angka1 / angka2;
                } else {
                    System.out.println("Error: Pembagian dengan nol tidak diizinkan.");
                    return Double.NaN;
                }
            default:
                System.out.println("Operasi tidak valid.");
                return Double.NaN;
        }
    }

    public static void cetakRiwayatKeFile(List<String> riwayat, BufferedWriter penulis) {
        try {
            System.out.println("Mencetak riwayat ke file riwayat.txt...");
            for (String operasi : riwayat) {
                penulis.write(operasi + "\n");
            }
            System.out.println("Riwayat telah dicetak ke dalam file riwayat.txt.");
        } catch (IOException e) {
            System.out.println("Gagal mencetak riwayat ke file riwayat.txt.");
        }
    }

    public static void tampilkanRiwayat(List<String> riwayat) {
        System.out.println("Riwayat operasi:");
        for (String operasi : riwayat) {
            System.out.println(operasi);
        }
    }

    public static List<String> bacaRiwayatDariFile(String namaFile) {
        List<String> riwayat = new ArrayList<>();
        try (BufferedReader pembaca = new BufferedReader(new FileReader(namaFile))) {
            String baris;
            while ((baris = pembaca.readLine()) != null) {
                riwayat.add(baris);
            }
        } catch (IOException e) {
            System.out.println("Gagal membaca dari file " + namaFile);
        }
        return riwayat;
    }

    public static double hitungTotal(List<String> riwayat) {
        double total = 0.0;
        for (String operasi : riwayat) {
            String[] bagian = operasi.split("=");
            if (bagian.length == 2) {
                try {
                    double hasilOperasi = Double.parseDouble(bagian[1].trim());
                    total += hasilOperasi;
                } catch (NumberFormatException e) {
                    continue;
                }
            }
        }
        return total;
    }
}

