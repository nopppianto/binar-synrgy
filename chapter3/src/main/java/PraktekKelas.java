import java.util.Objects;

public class PraktekKelas {
    public static void main(String[] args) {
        String name1 = "Novianto";
        String name2 = "Anggoro";
        String name3 = "No Name";

        String[] dataNama = {"Test Nama", name1, name2};

        Object[] objectData = {"123", 123};
        System.out.println(objectData[0].toString());
        System.out.println(objectData[1]);

        for (int i = 1; i <= 10; i++){
            System.out.println("Angka-"+ i);
        }

        int operAngka = 10;
        int operAngka2 = 10;
        int operAngka3 = 10;
        int operAngka4 = 10;

        System.out.println("data = " + (operAngka++));
        System.out.println("data = " + (++operAngka2));
        System.out.println("data = " + (--operAngka3));
        System.out.println("data = " + (operAngka4--));

        int a = 1;
        int b = 2;
        System.out.println(a == b);
        System.out.println(a > b);
        System.out.println(a >= b);
        System.out.println(a <= b);
        System.out.println(a < b);

        String data1= "Laptop";
        String data2 = "laptop";
        System.out.println(data1 == data2); // beda value
        System.out.println(data1.equals(1)); // beda tipe data
        System.out.println(Objects.equals(data1, 2)); // point 2 : beda tipe data
        System.out.println(data1.equals(String.valueOf(data2)));
    }
}
